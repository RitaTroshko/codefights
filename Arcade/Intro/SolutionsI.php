<?php
/*Several people are standing in a row and need to be divided into two teams. The first person goes into team 1, the second goes into team 2, the third goes into team 1 again, the fourth into team 2, and so on.

You are given an array of positive integers - the weights of the people. Return an array of two integers, where the first element is the total weight of team 1, and the second element is the total weight of team 2 after the division is complete.

Example

For a = [50, 60, 60, 45, 70], the output should be
alternatingSums(a) = [180, 105].*/

function alternatingSums($arr) {
    $sums = [0,0];
    foreach($arr as $i => $weight) {
        $sums[$i % 2] += $weight;
    }
    return $sums;
}

/*Write a function that reverses characters in (possibly nested) parentheses in the input string.

Input strings will always be well-formed with matching ()s.

Example

For inputString = "(bar)", the output should be
reverseInParentheses(inputString) = "rab";
For inputString = "foo(bar)baz", the output should be
reverseInParentheses(inputString) = "foorabbaz";
For inputString = "foo(bar)baz(blim)", the output should be
reverseInParentheses(inputString) = "foorabbazmilb";
For inputString = "foo(bar(baz))blim", the output should be
reverseInParentheses(inputString) = "foobazrabblim".
Because "foo(bar(baz))blim" becomes "foo(barzab)blim" and then "foobazrabblim".*/

function reverseInParentheses($inputString) {
    while (preg_match('/\(([^()]*)\)/', $inputString, $m))
        $inputString = str_replace($m[0], strrev($m[1]), $inputString);
    return $inputString;
}

/*Some people are standing in a row in a park. There are trees between them which cannot be moved. Your task is to rearrange the people by their heights in a non-descending order without moving the trees. People can be very tall!

Example

For a = [-1, 150, 190, 170, -1, -1, 160, 180], the output should be
sortByHeight(a) = [-1, 150, 160, 170, -1, -1, 180, 190].*/

function sortByHeight($a) {
    $keys = array_keys($a, -1);
    $sort = array_diff($a, [-1]);
    asort($sort);
    $sorted = array_values($sort);
    foreach($keys as $key)
    {
        array_splice($sorted, $key, 0, -1);
    }
    return $sorted;
}

/*Given two strings, find the number of common characters between them.

Example

For s1 = "aabcc" and s2 = "adcaa", the output should be
commonCharacterCount(s1, s2) = 3.

Strings have 3 common characters - 2 "a"s and 1 "c".*/

function commonCharacterCount($s1, $s2) {
    $s1 = str_split($s1);
    $s2 = str_split($s2);
    $c = 0;

    foreach($s1 as $key => $letter) {
        $key2 = array_search($letter, $s2);

        if($key2 !== false) {
            unset($s2[$key2]);
            $c++;
        }
    }
    return $c;
}

/*Given an array of strings, return another array containing all of its longest strings.

Example

For inputArray = ["aba", "aa", "ad", "vcd", "aba"], the output should be
allLongestStrings(inputArray) = ["aba", "vcd", "aba"].*/

function allLongestStrings($inputArray) {

    $strLengths = array_map('strlen', $inputArray);
    $keys = array_keys($strLengths, max($strLengths));
    $allLongestStrings = array_values(array_intersect_key($inputArray, array_flip($keys)));

    return $allLongestStrings;
}
